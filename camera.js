class SimpleCamera extends HTMLElement {
   connectedCallback() {
     const shadow = this.attachShadow({ mode: 'open' });
     this.videoElement = document.createElement('video');
     this.videoElement.setAttribute('autoplay', true);
     shadow.appendChild(this.videoElement);

     navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
     window.URL = window.URL || window.webkitURL;
     navigator.getUserMedia({video: true}, stream => {
      this.videoElement.srcObject = stream;
     },
     (e) => console.log('Camera did not work.', e)); 
   }
 }
 customElements.define('simple-camera', SimpleCamera);